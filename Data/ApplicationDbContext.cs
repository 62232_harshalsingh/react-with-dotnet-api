﻿using APIUploadImage.Model;
using Microsoft.EntityFrameworkCore;

namespace APIUploadImage.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<EmployeeModel> Employees { get; set; }
    }

}
