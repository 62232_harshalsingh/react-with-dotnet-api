﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace APIUploadImage.Migrations
{
    /// <inheritdoc />
    public partial class changeEmployeeName : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "occupation",
                table: "Employees",
                newName: "Occupation");

            migrationBuilder.RenameColumn(
                name: "Employee",
                table: "Employees",
                newName: "EmployeeId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Occupation",
                table: "Employees",
                newName: "occupation");

            migrationBuilder.RenameColumn(
                name: "EmployeeId",
                table: "Employees",
                newName: "Employee");
        }
    }
}
